// keyup isiliye liya tha kyuki hum jo bhi tyoe karte hai vo sab ho jata hai keyup pr 
// you cannot store java script ka object for this they came accross JSON
// JSON: Java Script Object Notation
// hum objects {} se banate hai n they habe key :value pair but in json your key is string and your value is either a string or a integer
// there is no method known as push on string hume json me convert karna padega JSON.stringify(arg)


// Define UI Elements variables

const form          =    document.querySelector("#task-form");
const taskList      =    document.querySelector("#collection-list");
const clearBtn      =    document.querySelector(".clear-tasks");
const filter        =    document.querySelector("#filter");
const taskInput     =    document.querySelector("#task");

loadEvents();

function loadEvents() {
    
    // Window Load Event
    document.addEventListener('DOMContentLoaded', getTasks); //DOMContentLoaded: ye method tab call hogi jab sab ho jayega parse ho jayega json ho jayega sab kuch not before that
    
    // FORM Submit event
    form.addEventListener('submit', addTask);
    
    // remove task event
    taskList.addEventListener('click', removeTask);
    
    // clear all tasks event
    clearBtn.addEventListener('click', clearTasks);
    
    // filter task event
    filter.addEventListener('keyup', filterTasks);  //keyup pe humara jitna bhi type karna hota hai sab ho jata hai
}
function createAndAddItemToTaskList(task) {
        // we have to create a new 'li' and insert it in 'ul'

        //create li element
        const li = document.createElement('li');
    
        //add class names
        li.className = "collection-item";
    
        // add text in li
        li.appendChild(document.createTextNode(task));

        //create a link for deleting
        const deleteLink = document.createElement('a');

        //add attribute to link
        deleteLink.setAttribute('href', '#');

        //add class
        deleteLink.className = "delete-item secondary-content";

        //add 'x' as a button in a
        deleteLink.innerHTML = "<i class='fa fa-remove'></i>";

        //append link to li
        li.appendChild(deleteLink);

        //append li to the task list (ul)
        taskList.appendChild(li);
}
function addTask(e) {
    e.preventDefault();
    if(taskInput.value === '') {
        alert("Please do insert any task!");
    } else {
        
        createAndAddItemToTaskList(taskInput.value);
        
        //Store to LocalStorage
        storeTaskInLocalStorage(taskInput.value);
        
        //clear the task input
        taskInput.value = "";
    }
}

function removeTask(e) {
    if(e.target.classList.contains('delete-item') || e.target.parentElement.classList.contains('delete-item')){
        
        if(confirm('Are you sure you wanna delete?')) {
            let taskValue;
            
            if(e.target.parentElement.nodeName === 'LI') {
                taskValue = e.target.parentElement.textContent;
                e.target.parentElement.remove();
            } else{
                taskValue = e.target.parentElement.parentElement.textContent;
                e.target.parentElement.parentElement.remove();
            }
            removeTaskFromLocalStorage(taskValue);
        }
    }
}

function clearTasks(e) {
    e.preventDefault();
    //slower method
    // taskList.innerHTML = '';
    
    //faster method
    while(taskList.firstChild) {
        taskList.removeChild(taskList.firstChild);
    }
    
    //remove all from localStorage
    let tasks =[];
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function filterTasks(e) {
    const key = e.target.value.toLowerCase();
    
    document.querySelectorAll('.collection-item').forEach(function(task) {
        const item = task.firstChild.textContent;
        if(item.toLowerCase().indexOf(key) == -1) {       //agar key nahi mila to key -1 hoga
            task.style.display = 'none';
        } else {
            task.style.display = 'block';
        }
    });
}

function storeTaskInLocalStorage(task) {
    let tasks;
    if(localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function getTasks(e) {
    //retrieve all the task from storage and display
    let tasks;
    if(localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    
    tasks.forEach(function(task){
        createAndAddItemToTaskList(task);
    });
}
    
function removeTaskFromLocalStorage(task) {   
    let tasks;
    if(localStorage.getItem('tasks') === null) {
        tasks = [];
    } else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    
    
    tasks.forEach(function(taskValue, index) {   //breaka nahi aata forEach me
        if(taskValue === task) {
            tasks.splice(index, 1);
        }
    });
    
    localStorage.setItem('tasks', JSON.stringify(tasks));  //updating our local storage
}


















